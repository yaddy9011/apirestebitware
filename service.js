
var http = require("http");
var express = require('express');
var app = express();
var connection = require('./conn');
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));


var port = process.env.PORT || 3000;
app.listen(port);
console.log('RESTful API server started on: ' + port);

//REST API GET
app.get('/cliente', function (req, res) {
    connection.query('SELECT * FROM clientes', function (error, rows, fields) {
        if (error) {
            console.log(error);
        } else {
            res.send(JSON.stringify(rows));
        }
    });
});

// REST API POST
app.post('/cliente', function (req, res) {
    var nombre = req.body.nombre;
    var apellidos = req.body.apellidos;
    var nombre_usurio = req.body.nombre_usuario;
    var correo_electronico = req.body.correo_electronico;
    var contraseña = req.body.contrasenia;
    var sql = "INSERT INTO clientes" +
        "(nombre,apellidos,nombre_usuario,correo_electronico,contraseña) VALUES " +
        "('" + nombre + "', '" + apellidos + "', '" + nombre_usurio + "', '" + correo_electronico + "', '" + contraseña + "')";
    connection.query(sql, function (error, results, fields) {
        if (error) throw error;
        res.end(JSON.stringify(results));
    });
});

//REST API PUT
app.put('/cliente', function (req, res) {
    var edad = req.body.edad;
    var estatura = req.body.estatura;
    var peso = req.body.peso;
    var GEB = req.body.GEB;
    var cliente_id = req.body.cliente_id;
    var sql = "UPDATE clientes SET edad = " + edad + ", estatura = " + estatura + ", peso = " + peso + ", GEB = " + GEB +
        " WHERE cliente_id = " + cliente_id;
    connection.query(sql, function (error, results, fields) {
        if (error) throw error;
        res.end(JSON.stringify(results));
    });
});


